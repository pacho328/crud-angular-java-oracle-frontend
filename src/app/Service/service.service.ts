import { Persona } from './../Modelo/Persona';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  Url= "http://localhost:8080/ejemplo01/personas";

  constructor(private http:HttpClient) {
   }
  public getPersonas(){
     return this.http.get<Persona[]>(this.Url);
   }
   public createPersona(persona:Persona){
     return this.http.post<Persona>(this.Url,persona);
   }
   public getPersonaId(id:number){
    return this.http.get<Persona>(this.Url+"/"+id)
   }
   public updatePersona(persona:Persona){
    return this.http.put<Persona>(this.Url+"/"+persona.id,persona);
   }
   public deletePersona(persona:Persona){
    return this.http.delete<Persona>(this.Url+"/"+persona.id);
   }
}

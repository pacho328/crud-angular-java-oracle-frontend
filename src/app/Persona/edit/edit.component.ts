import { ServiceService } from './../../Service/service.service';
import { Router } from '@angular/router';
import { Persona } from 'src/app/Modelo/Persona';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  persona: Persona;
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    this.editar()
  }
  public editar(){
    let id=localStorage.getItem("id");
    console.log(id);
    this.service.getPersonaId(+id).subscribe(data=>{
      this.persona = data;
    })
  }
  public actualizar(persona:Persona){
    this.service.updatePersona(persona).subscribe(data=>{
      this.persona=data;
      this.router.navigate(["listar"])
    });
  }
}



import { ServiceService } from './../../Service/service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/Modelo/Persona';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  persona: Persona ={
    id: 0,
    name: "",
    apellidos: ""
  }
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
  }
  public guardar(){
    console.log(this.persona);
    this.service.createPersona(this.persona).subscribe(
      data=>{
        alert("exito!!");
        this.router.navigate(["listar"])
      }
    )
  }
}

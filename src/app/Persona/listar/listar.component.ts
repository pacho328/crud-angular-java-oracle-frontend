import { ServiceService } from './../../Service/service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/Modelo/Persona';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  Personas: Persona[];
  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit(): void {
    this.service.getPersonas().subscribe(
      data =>{
        this.Personas =data;
      }
    );
  }
  public editar(persona:Persona){
    localStorage.setItem("id",persona.id.toString());
    this.router.navigate(["edit"]);
  }
  public delete(persona:Persona){
    console.log(persona.name);

    this.service.deletePersona(persona).subscribe(data=>{
      this.Personas = this.Personas.filter(p=>p!==persona);
      alert("eliminado")
      this.router.navigate(["listar"]);

    })

  }

}
